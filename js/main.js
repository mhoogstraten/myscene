'use strict';

Physijs.scripts.worker = './js/libs/physijs_worker.js';
Physijs.scripts.ammo = './ammo.js';

var main = new function()
{
    this.SCREEN_WIDTH = window.innerWidth;
    this.SCREEN_HEIGHT = window.innerHeight;
    this.VIEW_ANGLE = 45;
    this.ASPECT = this.SCREEN_WIDTH / this.SCREEN_HEIGHT;
    this.NEAR = 0.1;
    this.FAR = 20000;

    this.keyboard = new THREEx.KeyboardState();
    this.clock = new THREE.Clock();

    this.htmlContainer = null;

    this.scene = null;
    this.floor = null;
    this.renderer = null;
    this.controls = null;

    this.collidableMeshList = [];

    //models
    this.player = null;

    this.main = function()
    {
        this.setScene();
        this.setCamera();
        this.setRenderer();
        this.setEvents();
        this.setControls();
        this.setStats();
        this.setLight();
        this.setFloor();
        this.setSky();
        this.setPlayer();
        this.setLasers();
        this.animate();
    };
    this.animate = function()
    {
        main.runListener();
        main.scene.simulate();
        main.renderer.render( main.scene, main.camera );
        requestAnimationFrame( main.animate );
    };
    this.setLasers = function()
    {

    };
    this.runListener = function()
    {
        var delta = this.clock.getDelta(); // seconds.
        var moveDistance = 200 * delta; // 200 pixels per second
        var rotateAngle = Math.PI / 2 * delta;   // pi/2 radians (90 degrees) per second


        this.player.setDamping(0.0, 1.0);

        // move forwards/backwards/left/right
        this.player.__dirtyPosition = true;
        if ( this.keyboard.pressed("W") )
            this.player.translateZ( -moveDistance );
        if ( this.keyboard.pressed("S") )
            this.player.translateZ(  moveDistance );
        if ( this.keyboard.pressed("Q") )
            this.player.translateX( -moveDistance );
        if ( this.keyboard.pressed("E") )
            this.player.translateX(  moveDistance );

        // rotate left/right/up/down
        this.player.__dirtyRotation = true;
        if ( this.keyboard.pressed("A") )
            this.player.rotateOnAxis( new THREE.Vector3(0,1,0), rotateAngle);
        if ( this.keyboard.pressed("D") )
            this.player.rotateOnAxis( new THREE.Vector3(0,1,0), -rotateAngle);


        if ( this.keyboard.pressed("space") )
        {
            if(this.scene.position.y == Math.round(this.player.position.y+235) )
                this.player.translateY( this.scene.position.y + 10 );
        }

        this.camera.position.x = this.player.position.x;
        this.camera.position.y = this.player.position.y+150;
        this.camera.position.z = this.player.position.z+400;
        this.camera.lookAt( this.player.position );

        this.stats.update();
    };
    this.setPlayer = function()
    {
        var cubeGeometry = new THREE.CubeGeometry(10,30,10,1,1,1);
        var wireMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe:true, transparent: true, opacity: 0 } );
        this.player = new Physijs.BoxMesh(
            cubeGeometry,
            wireMaterial
        );
        this.player.__dirtyPosition = true;
        this.player.position.set(0, -130, 0);
        this.player.setDamping(0.8, 1.0);
        this.player.castShadow=true;
        this.loadModel();
        this.scene.add( this.player );
    };
    this.loadModel = function()
    {
        var manager = new THREE.LoadingManager();
        manager.onProgress = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };
        var texture = new THREE.Texture();
        var loader = new THREE.ImageLoader( manager );
        loader.load( 'images/textures/han/chest.png', function ( image ) {
            texture.image = image;
            texture.needsUpdate = true;
        } );
        loader = new THREE.OBJLoader( manager );
        loader.load( './objects/han.obj', function ( object ) {
            object.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material.map = texture;
                }
            } );
            object.rotateOnAxis( new THREE.Vector3(0,1,0), 3.2 );
            object.position.y = -15;
            main.player.add( object );
        } );
    };
    this.setRenderer = function()
    {
        if ( Detector.webgl )
            this.renderer = new THREE.WebGLRenderer( {antialias:true} );
        else
            this.renderer = new THREE.CanvasRenderer();
        this.renderer.setSize( this.SCREEN_WIDTH, this.SCREEN_HEIGHT );
        this.renderer.setClearColor( this.scene.fog.color );
        this.htmlContainer = document.getElementById( 'ThreeJS' );
        this.htmlContainer.appendChild( this.renderer.domElement );
    };
    this.setCamera = function()
    {
        this.camera = new THREE.PerspectiveCamera( this.VIEW_ANGLE, this.ASPECT, this.NEAR, this.FAR );
        this.scene.add( this.camera );
    };
    this.setScene = function()
    {
        this.scene = new Physijs.Scene;
        this.scene.setGravity(new THREE.Vector3( 0, -32, 0 ));
        this.scene.fog = new THREE.Fog( 0xcce0ff, 500, 10000 );
    };
    this.setEvents = function()
    {
        THREEx.WindowResize( this.renderer, this.camera );
        THREEx.FullScreen.bindKey({ charCode: 'm'.charCodeAt(0) });
    };
    this.setControls = function()
    {
        this.controls = new THREE.OrbitControls( this.camera, this.renderer.domElement );
    };
    this.setLight = function()
    {
        var light = new THREE.DirectionalLight( 0xFFFFFF );
        light.position.set( 0, 500, 0 );
        light.target.position.copy( this.scene.position );
        light.castShadow = true;
        light.shadowCameraLeft = -60;
        light.shadowCameraTop = -60;
        light.shadowCameraRight = 60;
        light.shadowCameraBottom = 60;
        light.shadowCameraNear = 20;
        light.shadowCameraFar = 200;
        light.shadowBias = -.0001
        light.shadowMapWidth = light.shadowMapHeight = 2048;
        light.shadowDarkness = .7;
        this.scene.add(light);
    };
    this.setStats = function()
    {
        this.stats = new Stats();
        this.stats.domElement.style.position = 'absolute';
        this.stats.domElement.style.bottom = '0px';
        this.stats.domElement.style.zIndex = 100;
        this.htmlContainer.appendChild( this.stats.domElement );
    };
    this.setFloor = function()
    {
        var initColor = new THREE.Color( 0x497f13 );
        var initTexture = THREE.ImageUtils.generateDataTexture( 1, 1, initColor );

        var groundMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x111111, map: initTexture } );

        var groundTexture = THREE.ImageUtils.loadTexture( "./images/textures/terrain/grasslight-big.jpg", undefined, function() { groundMaterial.map = groundTexture } );
        groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
        groundTexture.repeat.set( 25, 25 );
        groundTexture.anisotropy = 16;

        this.floor = new Physijs.PlaneMesh( new THREE.PlaneGeometry( 20000, 20000 ), groundMaterial );
        this.floor.position.y = -250;
        this.floor.rotation.x = - Math.PI / 2;
        this.floor.receiveShadow = true;
        this.floor.occ = true;
        this.scene.add( this.floor );

        document.addEventListener( 'mousemove', this.onDocumentMouseMove, false );
    };
    this.setSky = function()
    {
    };
    this.onDocumentMouseMove = function(event)
    {
        var projector = new THREE.Projector();
        var vector = new THREE.Vector3(  ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1, 1);
        projector.unprojectVector( vector, this.camera );

        var ray = new THREE.Ray( this.camera.position, vector.subSelf( this.camera.position ).normalize() );
        var intersect = ray.intersectObject( this.floor );

        if ( intersect.length > 0) {
            console.log(intersect[0].point);
            //document.getElementById('z').value = intersect[0].point.z;
            //document.getElementById('x').value = intersect[0].point.x;
        }
    };
};